var mysql = require("mysql");
var config = require('./config');

var mysqlcon = mysql.createConnection(config.CON);


mysqlcon.connect(function(err) {
  if (err) throw err; 
  console.log("Connected!");
});


module.exports = mysqlcon;
