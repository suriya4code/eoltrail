var express = require("express");
var mysqlcon = require("../db");
var mysqldbinit = require("../dbinit");
var router = express.Router();

/* GET home page. */
router.get("/", function(req, res, next) {
  mysqlcon.query("USE firstdbtrail1", function(err, result) {
    if (err) throw err;
    console.log("firstdbtrail1 database in use");
  });

  mysqlcon.query("select * from customers", (err, rows, fields) => {
    if (err) {
      console.log(err);
      return;
    } else res.json(rows);
    //res.render('index', { title: 'EolApi' });
  });
});


router.get('/start_script/FIDIEOL',(req,res)=>{

  mysqldbinit.call();
  res.send("Completed");

});

module.exports = router;
