var express = require("express");
var mysqlcon = require("../db");
var EOLLOGTBL = require("../models/elogmodel");
var router = express.Router();

router.get("/", function(req, res, next) {
  res.status(200).send("Please check the method type :)");
});

router.get("/log/details", function(req, res, next) {
  mysqlcon.query("select * from eollogging.tbleollog", (err, rows, fields) => {
    if (err) {
      res.status(400).send(err.sqlMessage);
    } else res.json(rows);
  });
});

var sqlpost =
  "INSERT INTO `eollogging`.`tbleollog` \
(`logid`, \
`deviceid`,  \
`gps`,  \
`gsm`,  \
`externalbattery`, \
`internalbattery`, \
`ignition`, \
`subscriptionstatus`, \
`userdetails`, \
`latitude`, \
`longitude`) \
VALUES  \
('0',?, ? , ?, ?, ?, ?, ?, ?,?,?)";

router.post("/log", function(req, res, next) {
  mysqlcon.query("USE eollogging", function(err, result) {
    if (err) throw err;
    console.log("eollogging database in use");
  });

  mysqlcon.query(
    sqlpost,
    [
      req.body.deviceid,
      req.body.gps,
      req.body.gsm,
      req.body.externalbattery,
      req.body.internalbattery,
      req.body.ignition,
      req.body.subscriptionstatus,
      req.body.userdetails,
      req.body.latitude,
      req.body.longitude
    ],
    (err, rows, fields) => {
      if (err) {
        res.status(400).send(err.sqlMessage);
      } else {
        if (rows.affectedRows > 0) {
          res.status(200).send("success");
        } else {
          res.status(400).send(rows.message);
        }
      }
    }
  );
});

module.exports = router;
