var mysqlcon = require("./db");

var dbinitialize = function () {
  //  (1) ---- creating DB
  var createdbquery = "CREATE DATABASE IF NOT EXISTS eollogging";
  //executing create db command in db
  mysqlcon.query(createdbquery, function(err, result) {
    if (err) throw err;
    console.log("eollogging Database created");
    console.log(result);
  });

  //  (2) ---- using DB
  mysqlcon.query("USE eollogging", function(err, result) {
    if (err) throw err;
    console.log("eollogging in use");
  });

  // (3) ---- Creating Table

  // CREATE TABLE  IF NOT EXISTS
  //       eollog    (logid INT AUTO_INCREMENT PRIMARY KEY,
  //   deviceid VARCHAR(50) NOT NULL,
  //   gps CHAR(1),
  //   gsm CHAR(1),
  //   externalbattery DECIMAL(5,2),
  //   internalbattery DECIMAL(5,2),
  //   ignition BOOLEAN,
  //   subscriptionstatus VARCHAR(50) NULL ,
  //   userdetails VARCHAR(255) NULL,
  //   latitude DECIMAL(10, 8)  NOT NULL,
  //   longitude DECIMAL(11, 8) NOT NULL,
  //   )"

  var createtblquery =
    "CREATE TABLE eollogging.tbleollog ( \
      `logid` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, \
      `deviceid` varchar(50) NOT NULL, \
      `gps` char(1) DEFAULT NULL, \
      `gsm` char(1) DEFAULT NULL, \
      `externalbattery` decimal(5,2) DEFAULT NULL, \
      `internalbattery` decimal(5,2) DEFAULT NULL, \
      `ignition` tinyint(1) DEFAULT NULL, \
      `subscriptionstatus` varchar(50) DEFAULT NULL, \
      `userdetails` varchar(255) DEFAULT NULL, \
      `latitude` decimal(10,8) NOT NULL, \
      `longitude` decimal(11,8) NOT NULL \
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";

  mysqlcon.query(createtblquery, function(err, result) {
    if (err) throw err;
    console.log("tbleollog TABLE created");
    console.log(result);
  });
}

module.exports = dbinitialize;